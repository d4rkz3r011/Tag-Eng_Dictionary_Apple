//
//  ViewController.swift
//  Tag-Eng Dictionary
//
//  Created by Nathan Geronimo on 6/26/17.
//  Copyright © 2017 Nathan Geronimo. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    // USE CoreData EXAMPLE TO CREATE A DATABASE
    
    //MARK: Properties
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var searchBar: UITextField!
    @IBOutlet weak var definitionText: UILabel!
    @IBOutlet weak var wordDefinition: UILabel!
    @IBOutlet weak var searchButton: UIButton!
    
    //MARK: Variables
    var englishDictionary: [String:Set<String>] = [:] //Create empty dictionary
    var tagalogDictionary: [String:Set<String>] = [:]
    
    //MARK: Overriden functions
    override func viewDidLoad() {
        loadEnglishCSV()
        loadTagalogCSV()
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //Load both dictionaries
//        loadEnglishDictionary() //Load in all the words from text file
//        loadTagalogDictionary() //Load in all the words from text file

        
        //This allows hiding the keyboard when tapping outside of it.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tap)
        
        //Adjust label according to the screen size
        topLabel.numberOfLines = 2 //Make the label go to two lines
        topLabel.adjustsFontSizeToFitWidth = true //Adjust font size if needed
        
//        definitionText.lineBreakMode = .byWordWrapping
//        definitionText.numberOfLines = 0
        definitionText.text = "** word **"
        
        wordDefinition.lineBreakMode = .byWordWrapping //Allows definition to go past one line
        wordDefinition.numberOfLines = 0 //Allows the label to expand to an unlimited amount of lines
        wordDefinition.text = "** definition **"
        
        searchBar.delegate = self //Use delegate to manage user input
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder() //Hides the keyboard
        search(searchButton)
        return true;
    }

    //MARK: Actions
    @IBAction func search(_ sender: UIButton) {
        searchWord(searchBar.text!.lowercased().trimmingCharacters(in: .whitespaces))
    }
    
    /*
     * This searches for the word in a dictionary.
     * If the word exists, then the definition will be displayed on the app
     */
    func searchWord(_ searchText: String) {
        definitionText.text = searchBar.text! //Puts the word in bold
        wordDefinition.text! = "" //Clear the definition text just in case

        //what is there's more than one word with the same meaning due to pronounciation?
        if englishDictionary[searchText] != nil {
            let setSize = englishDictionary[searchText]!.count //The size of the set
            var iteration = 0 //Tracks the interation count of the for-in loop
            for definition in englishDictionary[searchText]! {
                wordDefinition.text! += definition //Definitions will always be added on to the string, not replaced.
                if (setSize > 1 && iteration < setSize - 1) { //If the set has more than 1 definition, and isn't the last definition,
                    wordDefinition.text! += "; " //place a semicolon after the word when printing out
                }
                iteration += 1
            }
        }
        else if tagalogDictionary[searchText] != nil {
            let setSize = tagalogDictionary[searchText]!.count //The size of the set
            var iteration = 0 //Tracks the interation count of the for-in loop
            for definition in tagalogDictionary[searchText]! {
                wordDefinition.text! += definition //Definitions will always be added on to the string, not replaced.
                if (setSize > 1 && iteration < setSize - 1) { //If the set has more than 1 definition, and isn't the last definition,
                    wordDefinition.text! += "; " //place a semicolon after the word when printing out
                }
                iteration += 1
            }
        }
        else {
            wordDefinition.text = "** Word isn't defined in dictionary **" // No definition will be visible
        }

    }
    
    /*
     * Loads the Engish dictionary variable from the text file
     */
    func loadEnglishCSV() {
        //First check if the file is read by the app, if not it ends the function
        guard let ePath = Bundle.main.path(forResource: "English_To_Tagalog", ofType: ".csv")
            else { return }
        
        do {
            let csvData = try String(contentsOfFile: ePath, encoding: .utf8)
            let csv = csvData.csvRows()                                 //Get all the rows from the csv file and put it into an array
            for row in csv {                                            //for each row in the csv file,
                let word = row[0].trimmingCharacters(in: .whitespaces)  //The word we will define
                let definition = row[1].trimmingCharacters(in: .whitespaces)
                var definitions = Set<String>()
                definitions.insert(definition)                          //The meaning of the word
                if (englishDictionary[word] == nil) {                   //If the word doesn't have a definition in the dictionary yet,
                    englishDictionary[word] = definitions               //Add the word and meaning to the dictionary
                }
                else {                                                  //If the word already has a definition,
                    englishDictionary[word]?.formUnion(definitions)     //Add another definition
                }
            }
        } catch {
            print(error)
        }
        
    }
    
    /*
     * Loads the Tagalog dictionary variable from the text file
     */
    func loadTagalogCSV() {
        //First check if the file is read by the app, if not it ends the function
        guard let ePath = Bundle.main.path(forResource: "Tagalog_To_English", ofType: ".csv")
            else { return }
        
        do {
            let csvData = try String(contentsOfFile: ePath, encoding: .utf8) //Put the contents of the csv in a variable
            let csv = csvData.csvRows()                     //Get all the rows from the csv file and put it into an array
            for row in csv {                                //for each row in the csv file,
                let word = row[0]                           //The word we will define
                var definitions = Set<String>()
                definitions.insert(row[1])                  //The meaning of the word
                if (tagalogDictionary[word] == nil) {       //If the word doesn't have a definition in the dictionary yet,
                    tagalogDictionary[word] = definitions   //Add the word and meaning to the dictionary
                }
                else {                                      //If the word already has a definition,
                    tagalogDictionary[word]?.formUnion(definitions) //Add another definition
                }
            }
        } catch {
            print(error)
        }
    }
    
    /*
     * Loads the Engish dictionary variable from the text file
     */
//    func loadEnglishDictionary() {
//        do {
//            var index = 0
//            let engPath = Bundle.main.path(forResource: "English_To_Tagalog", ofType: "txt") //Grab the file path
//            let engData = try String(contentsOfFile: engPath!, encoding: .utf8) //Load the file
//
//            for _ in engData.components(separatedBy: .newlines) { //Loop through every line in the file
//                let line = engData.components(separatedBy: "\n") //Grab each line in the file by new lines
//
//                let word1 = line[index].components(separatedBy: ": ")[0] //Grab the English word
//                if word1 == "" { break; } //Forces to exit loop. For some reason swift is grabbing an empty character in the file
//
//                let word2 = line[index].components(separatedBy: ": ")[1] //Grab the Tagalog translation
//                englishDictionary[word1] = word2 //Set English word as key to the Tagalog translation
//                index += 1 //increment the index to get the next word
//            }
//        }
//        catch { //Catch just in case
//            print("ERROR")
//        }
//    }
    
    /*
     * Loads the Tagalog dictionary variable from the text file
     */
//    func loadTagalogDictionary() {
//        do {
//            var index = 0
//            let tagPath = Bundle.main.path(forResource: "Tagalog_To_English", ofType: "txt") //Grab the file path
//            let tagData = try String(contentsOfFile: tagPath!, encoding: .utf8) //Load the file
//
//            for _ in tagData.components(separatedBy: .newlines) { //Loop through every line in the file
//
//                let line = tagData.components(separatedBy: "\n") //Grab each line in the file by new lines
//
//                let word1 = line[index].components(separatedBy: ": ")[0] //Grab the Tagalog word
//                if word1 == "" { break; } //Forces to exit loop. For some reason swift is grabbing an empty character in the file
//
//                let word2 = line[index].components(separatedBy: ": ")[1] //Grab the English translation
//                tagalogDictionary[word1] = word2 //Set Tagalog word as key to the English translation
//                index += 1 //increment the index to get the next word
//            }
//        }
//        catch { //Catch just in case
//            print("ERROR")
//        }
//    }
    
    /*
     * Hides the keyboard
     */
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
}

